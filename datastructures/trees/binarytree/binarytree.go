package bintree

import (
	"fmt"
)

type node struct {
	data  int
	left  *node
	right *node
}

type Tree struct {
	root *node
}

// Tree is implemention of tree Datastructure
func (t *Tree) Insert(data int) {
	if t.root == nil {
		t.root = &node{data, nil, nil}
		return
	} else {
		t.root.insert(data)
	}
}

// In function does in-order traversal on a Tree
func (t *Tree) In() {
	t.root.in()
}

// Pre function does pre-order traversal on a Tree
func (t *Tree) Pre() {
	t.root.pre()
}

// Post function does post-order traversal on a Tree
func (t *Tree) Post() {
	t.root.post()
}

// Search any element on a Tree
func (t *Tree) Search(q int) bool {
	return t.root.search(q)
}

func (n *node) insert(data int) {
	if n == nil {
		return
	}
	if data < n.data {
		if n.left == nil {
			n.left = &node{data, nil, nil}
		} else {
			n.left.insert(data)
		}
		return
	}
	if n.right == nil {
		n.right = &node{data, nil, nil}
	} else {
		n.right.insert(data)
	}
}

func (n *node) search(data int) bool {
	if n == nil {
		return false
	}
	if data == n.data {
		return true
	}
	if data < n.data {
		return n.left.search(data)
	}
	return n.right.search(data)
}

func (n *node) in() {
	if n == nil {
		return
	}
	fmt.Printf("%d\t", n.data)
	n.left.in()
	n.right.in()
}

func (n *node) pre() {
	if n == nil {
		return
	}
	if n.left != nil {
		n.left.pre()
	}
	fmt.Printf("%d\t", n.data)
	if n.right != nil {
		n.right.pre()
	}
}

func (n *node) post() {
	if n == nil {
		return
	}
	if n.right != nil {
		n.right.post()
	}
	fmt.Printf("%d\t", n.data)
	if n.left != nil {
		n.left.post()
	}
}
